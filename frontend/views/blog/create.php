<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Entries */

$this->title = 'Делитесь опытом. Ведь так заподился Open source';
$this->params['breadcrumbs'][] = ['label' => 'Блог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entries-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('forms/_form', [
        'entry' => $entry,
    'author' => $author,
    ]) ?>

</div>
