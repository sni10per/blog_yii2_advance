<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\yii2\recaptcha\ReCaptcha;
/* @var $this yii\web\View */
/* @var $model frontend\models\Entries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entries-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
    <?= $form->field($author, 'name', [ 'options' => ['class' => 'col-md-6 form-group']])->textInput()->label()?>
    <?= $form->field($author, 'mail', [ 'options' => ['class' => 'col-md-6 form-group']])->textInput()->label()?>
    </div>
            
    <?= $form->field($entry, 'title')->textInput()->label()?>
    <?= $form->field($entry, 'text')->textarea(['maxlength' => 1000, 'rows' => 7, 'cols' => 100])->label()?>

    <?= $form->field($author, 'reCaptcha', [
        'options' => [
            'class' => 'captcha'
        ]
    ])->widget(ReCaptcha::className(), ['siteKey' => '6LfsLiITAAAAAMAE-NUu40guBHgQQJzrhLmZMeIj'])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton( 'Можно бесплатно что-то написать' , ['class' =>  'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
