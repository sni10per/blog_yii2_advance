<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use himiklab\yii2\recaptcha\ReCaptcha;
/* @var $this yii\web\View */
/* @var $author frontend\models\Entries */
/* @var $comment yii\widgets\ActiveForm */
?>

<div class=" entries-form">

    <?php $form = ActiveForm::begin([
        'action' => ['blog/create-comment'],
    ]); ?>
    <?= $form->field($comment, 'entries')->hiddenInput()->label(false)?>
    <div class="row">
        <?= $form->field($author, 'name', [ 'options' => ['class' => 'col-md-6 form-group']])->textInput()->label()?>
        <?= $form->field($author, 'mail', [ 'options' => ['class' => 'col-md-6 form-group']])->textInput()->label()?>
    </div>

    
    <?= $form->field($comment, 'text')->textarea(['maxlength' => 1000, 'rows' => 7, 'cols' => 100])->label()?>


    <?= $form->field( $author, 'reCaptcha', [
        'options' => [
            'class' => 'captcha'
        ]
    ])->widget( ReCaptcha::className(), ['siteKey' => '6LfsLiITAAAAAMAE-NUu40guBHgQQJzrhLmZMeIj'])->label(false) ?>



    <div class="form-group">
        <?= Html::submitButton( 'Добавить свой бесценный комментарий', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
