<?php

use yii\helpers\Html;
use frontend\components\ExtendListView\ExtendListView;


/* @var $this yii\web\View */
/* @var $model frontend\models\Entries */

$this->title = $model->entry->title;
$this->params['breadcrumbs'][] = ['label' => 'Блог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entries-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p> Автор <b><?= Html::encode($model->author0->name) ?></b> написал это 
        <?= \Yii::$app->formatter->asDatetime( $model->created_at , "php:d.m.Y" ) ?>
        в <?= \Yii::$app->formatter->asDatetime( $model->created_at , "php:H:i:s " ) ?>
    </p>
   <p>
       <?= $model->entry->text ?>
    </p>

</div>


<div class="row" style="
    padding-top: 30px;
        margin-top: 25px;
    border-top: 3px solid #ddd;
">
        <?= ExtendListView::widget([
            'options' => [
                'class' => ' items',
            ],
            'layout' => "{summary}{sorter}\n{items}\n{pager}",
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model) {
                return $this->render('items/comment_item', [
                    'model' => $model
                ]);

            },
        ]) ?>
    </div>


    <div class="row" style="
    padding-top: 40px;
        margin-top: 25px;
    border-top: 3px solid #ddd;
">
    <div class="col-lg-10 col-lg-offset-1">
        <p>Есть что сказать? Не стесняйтесь ;) </p>
<?= $this->render('forms/_formcomment', [
    'comment' => $comment,
    'author' => $author,
    'model' => $model,
]) ?>
    </div>
    </div>
