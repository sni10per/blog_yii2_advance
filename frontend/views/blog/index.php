<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EntriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Use. Test. Enjoy ?>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entries-index">

    <div class="row">
    <div class="col-md-4">
        <h1 class="titles"><?= Html::encode($this->title) ?></h1>
    </div>
        <div class="col-md-8">
            <p style="    padding-top: 16px;     text-align: right;">
                <?= Html::a('Создать запись', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>

    <div class="container">
        <div class="col-xs-12">

            <div class="page-header">
                <h3>Популярное Очевидное Невероятное</h3>
                <p>Это должен обсудить каждый...</p>
            </div>

            <div class="carousel slide" id="myCarousel">
                    <?= ListView::widget([
                        'dataProvider' => $dataProviderSlider,
                        'summary' => false,
                        'layout' => "{items}",
                        'options' => [
                            'class' => 'carousel-inner',
                            'role' => 'listbox',
                        ],
                        'itemOptions' => ['class' => 'item'],
                        'itemView' => function ($model) {
                            return $this->render('items/slide_item.php', [
                                'model' => $model
                            ]);
                        },
                    ]) ?>
                <nav>
                    <ul class="control-box pager">
                        <li><a data-slide="prev" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
                        <li><a data-slide="next" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></a></li>
                    </ul>
                </nav>
                <!-- /.control-box -->
            </div><!-- /#myCarousel -->
        </div><!-- /.col-xs-12 -->
    </div><!-- /.container -->

    <div class="page-header">
        <h3>Последние поступления</h3>
        <p>Можно заглянуть, бывает интересно</p>
    </div>
    <div class="row" style="    padding-top: 52px;">

        <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model) {
            return $this->render('items/item_list.php', [
                'model' => $model
            ]);
        },
    ]) ?>
    </div>

</div>
