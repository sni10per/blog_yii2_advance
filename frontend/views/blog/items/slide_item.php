<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 09.06.2016
 * Time: 18:19
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Entries */



?>

    <ul class="thumbnails">
        <li class="col-sm-12">
            <div class="fff">
                <div class="thumbnail">
                    <a href="#"><img src="http://placehold.it/1110x240" alt=""></a>
                </div>
                <div class="caption">
                    <h4><?= $model->entry->title  ?></h4>
                    <p><?= \yii\helpers\StringHelper::truncateWords($model->entry->text, 50,'...') ?></p>
                    <a class="btn btn-danger" href="<?= \yii\helpers\Url::to(['blog/view', 'id' => $model->id ])  ?>">Великий HollyWar - Комментариев: <b><?= Html::encode( count($model->comments) )?></b> </a>
                </div>
            </div>
        </li>
    </ul>
