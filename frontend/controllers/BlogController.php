<?php

namespace frontend\controllers;

use frontend\models\forms\AuthorForm;
use frontend\models\Authors;
use frontend\models\Comment;
use frontend\models\Comments;
use frontend\models\CommentsSearch;
use frontend\models\Entry;
use frontend\models\forms\CommentForm;
use frontend\models\forms\EntryForm;
use Yii;
use frontend\models\Entries;
use frontend\models\EntriesSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BlogController implements the CRUD actions for Entries model.
 */
class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Entries models.
     * Вывести две модели записей по 2м критериям
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EntriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProviderSlider = $searchModel->searchSlider(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderSlider' => $dataProviderSlider,
        ]);
    }

    /**
     * Displays a single Entries model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        /**
         * @var $comment \frontend\models\forms\CommentForm
         */
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $comment = new CommentForm();
        $author = new AuthorForm();
        $comment->entries = $model->id;

        $searchModel = new CommentsSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge($request->queryParams, ['entires' => $model->id]));

        return $this->render('view', [
            'model' => $model,
            'comment' => $comment,
            'author' => $author,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Entries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * Так жа для начала и обеспечения целостности получаем необходимые составные части для строки записи блога
     * И после успешных проверок содержимого и автора создаем саму запись блога.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $entry = new EntryForm();
        $author = new AuthorForm();

        if ($entry->load($request->post()) && $entry->validate() && $author->load($request->post()) && $author->validate()) {

            $entries = new Entries();
            $entries->fillEntires($entry, $author);

            if ($entries)
                return $this->redirect(Url::to(['blog/view', 'id' => $entries->id]));
        }

        return $this->render('create', [
            'entry' => $entry,
            'author' => $author,
        ]);

    }

    /**
     * Creates a new Entries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * Контроллер отображения записи блога должен отвечать только за отображение записи блога и связанной
     * информацией с ним.
     * Генерацией комментария должен заниматься специализарованный элемент. Мухи отдельно - котлеты отдельно.
     * Сюда будем посылать данные с формы коммента чтобы не толкаться в actionView
     * @return mixed
     */
    public function actionCreateComment()
    {
        $request = Yii::$app->request;
        $comment = new CommentForm();
        $author = new AuthorForm();

        if ($comment->load($request->post()) && $comment->validate() && $author->load($request->post()) && $author->validate()) {

            $comments = new Comments();
            $comments->fillCommentsRow($comment, $author);

            if ($comments)
                return $this->redirect(Url::to(['blog/view', 'id' => $comments->entries]));
        }
        return $this->render('create', [
            'comment' => $comment,
            'author' => $author,
        ]);

    }


    /**
     * Finds the Entries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entries::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
