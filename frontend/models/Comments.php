<?php

namespace frontend\models;

use frontend\models\forms\AuthorForm;
use frontend\models\forms\CommentForm;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $entries
 * @property integer $author
 * @property integer $parent
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $comment_id
 *
 * @property Entries $entries0
 * @property Authors $author0
 * @property Comments $parent0
 * @property Comments[] $comments
 * @property Comment $comment
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entries', 'author', 'comment_id'], 'required'],
            [['entries', 'author', 'comment_id'], 'integer'],
            [['entries'], 'exist', 'skipOnError' => true, 'targetClass' => Entries::className(), 'targetAttribute' => ['entries' => 'id']],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => Authors::className(), 'targetAttribute' => ['author' => 'id']],
            [['parent'], 'default', 'value' => null],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['comment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entries' => 'Entries',
            'author' => 'Author',
            'parent' => 'Parent',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'comment_id' => 'Comment ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntries0()
    {
        return $this->hasOne(Entries::className(), ['id' => 'entries']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor0()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(Comments::className(), ['id' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function fillCommentsRow( CommentForm $commentForm, AuthorForm $authorForm)
    {
        /**
         * @var $newEntry \frontend\models\Comment
         * @var $comment \frontend\models\forms\CommentForm
         * Здесь мы уверены что обе приходящие формы отвалидированы еще в контроллере. И можем создать ядро коммента и "пристегнуть" детали
         */

        $authorId = $authorForm->checkAuthor();
        $comment = $commentForm->insertComment();
        
        if ( $comment && $authorId ) {
            $this->entries = $comment->entries;
            $this->author = $authorId;
            $this->comment_id = $comment->id;
        }

        return  ($this->validate() && $this->save()) ? $this : false;
    }


}
