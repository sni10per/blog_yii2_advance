<?php

namespace frontend\models\forms;

use frontend\models\Comment;
use yii\base\Model;
use yii\helpers\HtmlPurifier;

/**
 * Password reset form
 *  *
 * @property integer $id
 * @property string $text
 * @property integer $entries
 */
class CommentForm extends Model
{
    public $text;
    public $entries;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required', 'message' => 'нужно заполнить. Давайте жить дружно.'],
            [['text'], 'string', 'max' => 1000 , 'message' => 'Слишком толсто. Не более 1000 знаков'],
            [['entries'], 'integer'],
            [['entries'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Ваши идеи превосходны ',
            'entries' => 'System ',
        ];
    }


    /**
     * @return integer || bool
     */
    public function insertComment()
    {
        /**
         * @var $newEntry \frontend\models\Comment
         */
        $newComment = new Comment();
        $newComment->text = HtmlPurifier::process($this->text);
        $newComment->entries = $this->entries;

        return $newComment->save() ? $newComment : false;
    }



}
