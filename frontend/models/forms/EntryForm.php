<?php

namespace frontend\models\forms;

use frontend\models\Entry;
use Yii;
use yii\base\Model;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "entry".
 *
 * @property integer $id
 * @property string $text
 * @property string $title
 *
 * @property Entries $entries
 */
class EntryForm extends Model
{

    public $title;
    public $text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'title'], 'required', 'message' => 'нужно заполнить. Давайте жить дружно.'],
            [['text'], 'string', 'max' => 1000 , 'message' => 'Слишком толсто. Не более 1000 знаков'],
            [['title'], 'string', 'max' => 255 , 'message' => 'Слишком толсто. Не более 150 знаков'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Содержание ',
            'title' => 'Заголовок ',
        ];
    }

    public function insertEntry()
    {
        /**
         * @var $newEntry \frontend\models\Entry
         */
        $newEntry = new Entry();
        $newEntry->title = $this->title;
        $newEntry->text = $this->text;

        return $newEntry->save() ? $newEntry->id : false;
    }
    
    
}
