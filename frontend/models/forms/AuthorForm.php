<?php
namespace frontend\models\forms;

use frontend\models\Authors;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use yii\base\Model;
use yii\helpers\HtmlPurifier;

/**
 * ContactForm is the model behind the contact form.
 * @property string $mail
 * @property string $name
 * @property string reCaptcha
 * @var $newAuthor \frontend\models\Authors
 */
class AuthorForm extends Model
{
    public $name;
    public $mail;
    public $reCaptcha;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail','reCaptcha'], 'required', 'message' => 'нужно заполнить. Давайте жить дружно.' ],
            [['name', 'mail'], 'filter', 'filter' => 'trim', 'skipOnArray' => true ],
            [['mail'], 'email', 'message' => 'должен быть похож на Email' ],
            [['name'], 'default', 'value' => function ($model, $attribute) {
                return empty($model->$attribute) ? 'Многоликий никто' : $model->$attribute ;
            }],
            [['reCaptcha'], ReCaptchaValidator::className(), 'secret' => '6LfsLiITAAAAABC5BuFMLREKZ8RxCpG89pRXfZu_']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mail' => 'Email ',
            'name' => 'Ваше имя ',
        ];
    }


    public function checkAuthor()
    {
        /**
         * @var $newAuthor \frontend\models\Authors
         * @var $authorExist \frontend\models\Authors
         */
        $authorExist = Authors::find()->where([ 'mail' => $this->mail ])->one();
        if ( $authorExist ) {
            
            return $authorExist->id;
        } else {
            
            $newAuthor = new Authors();
            $newAuthor->mail = $this->mail ;
            $newAuthor->name = $this->name ;
            
            return $newAuthor->save() ? $newAuthor->id : false;
        }
    }

}
