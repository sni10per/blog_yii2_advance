<?php

namespace frontend\models;

use frontend\models\forms\AuthorForm;
use frontend\models\forms\EntryForm;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "entries".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author
 * @property integer $entry_id
 *
 * @property Comments[] $comments
 * @property Authors $author0
 * @property Entry $entry
 */
class Entries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entries';
    }

    public function behaviors()
    {
        return [ 
            TimestampBehavior::className(),
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'author', 'entry_id'], 'required'],
            [[ 'author', 'entry_id'], 'integer'],
            [['entry_id'], 'unique'],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => Authors::className(), 'targetAttribute' => ['author' => 'id']],
            [['entry_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entry::className(), 'targetAttribute' => ['entry_id' => 'id']],
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['entries' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor0()
    {
        return $this->hasOne(Authors::className(), ['id' => 'author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntry()
    {
        return $this->hasOne(Entry::className(), ['id' => 'entry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function fillEntires( EntryForm $entryForm, AuthorForm $authorForm)
    {
        // Здесь мы уверены что обе приходящие формы отвалидированы. И можем создать ядро записи и "пристегнуть" детали
        $authorId = $authorForm->checkAuthor();
        $entryId = $entryForm->insertEntry();

        if ( $entryId && $authorId ) {
            $this->entry_id = $entryId;
            $this->author = $authorId;
        }

        return  ($this->validate() && $this->save()) ? $this : false;
    }

}
