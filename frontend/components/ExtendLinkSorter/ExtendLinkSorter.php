<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 08.06.2016
 * Time: 21:51
 */

namespace frontend\components\ExtendLinkSorter;

use yii\helpers\Html;
use yii\widgets\LinkSorter;

class ExtendLinkSorter extends LinkSorter
{

    protected function renderSortLinks()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        $links = [];
        foreach ($attributes as $name) {
            $links[] = $this->sort->link($name, $this->linkOptions);
        }

        $result = Html::tag('div', Html::ul( $links, array_merge($this->options, ['encode' => false])), ['class' => 'sorter col-md-5' ] );

        return  $result;
    }

}